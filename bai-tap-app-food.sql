USE app_food;

DROP TABLE IF EXISTS like_res,
					 rate_res,
					 restaurants, 
					 orders, 
					 users, 
					 sub_foods, 
					 foods, 
					 food_types;

-- food_types TABLE
CREATE TABLE food_types (
type_id    	int 			auto_increment,
type_name  	varchar(255) 	not null,

PRIMARY KEY (type_id)
);

-- foods TABLE
CREATE TABLE foods (
food_id   	int 			auto_increment,
food_name 	varchar(255) 	not null,
image 	  	varchar(255) 	not null,
price 	  	float 	    	not null,
`desc` 	  	varchar(255) 	not null,
type_id    	int 			not null,

PRIMARY KEY (food_id),
FOREIGN KEY (type_id) references food_types(type_id)
);

-- sub_foods TABLE
CREATE TABLE sub_foods (
sub_id 		int 			auto_increment,
sub_name 	varchar(255) 	not null,
sub_price 	float 			not null,
food_id 	int 			not null,

PRIMARY KEY (sub_id),
FOREIGN KEY (food_id) references foods(food_id)
);

-- users TABLE
CREATE TABLE users (
user_id 	int 			auto_increment,
full_name 	varchar(255) 	not null,
email 		varchar(255) 	not null,
`password` 	varchar(255) 	not null,

PRIMARY KEY (user_id)
);

-- orders TABLE
CREATE TABLE orders (
user_id 	int 			not null,
food_id		int 			not null,
amount 		int 			not null,
`code`		varchar(255), 
arr_sub_id	varchar(255),

FOREIGN KEY (user_id) references users(user_id),
FOREIGN KEY (food_id) references foods(food_id)
);

-- restaurants TABLE
CREATE TABLE restaurants (
res_id		int				auto_increment,
res_name	varchar(255)	not null,
image		varchar(255)	not null,
`desc`		varchar(255)	not null,

PRIMARY KEY (res_id)
);

-- rate_res TABLE
CREATE TABLE rate_res (
user_id		int				not null,
res_id		int				not null,
amount		int				not null,
date_rate	datetime		not null,

FOREIGN KEY (user_id) references users(user_id),
FOREIGN KEY (res_id)  references restaurants(res_id)
);

-- like_res TABLE
CREATE TABLE like_res (
user_id		int				not null,
res_id		int				not null,
date_like	datetime		not null,

FOREIGN KEY (user_id) references users(user_id),
FOREIGN KEY (res_id)  references restaurants(res_id)
);


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Insert data -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --


-- food_types INSERT
INSERT INTO food_types (
type_name
) 
VALUE ("starters"),
	  ("salads"),
	  ("main courses"),
	  ("desserts"),
	  ("drinks");

-- foods INSERT
INSERT INTO foods (
food_name, image, price, `desc`, type_id
)
VALUE ("Tomato Soup", "tomato-soup.jpg", 4.99, "...", 1),
	  ("Chicken Soup", "chicken-soup.jpg", 4.99, "...", 1),
	  ("Guacamole Salad", "guacamole-salad.jpg", 6.00, "...", 2),
	  ("Grilled Fish and Potatoes", "grilled-fish-and-potatoes.jpg", 10.00, "...", 3),
	  ("Turkey and Ham Pie", "turkey-and-ham-pie.jpg", 12.99, "...", 3),
	  ("Vegetable Pasta", "vegatable-pasta.jpg", 10.00, "...", 3),
	  ("Ice Cream", "ice-cream.jpg", 2.50, "...", 4),
	  ("Tea", "tea.jpg", 2.00, "...", 5),
	  ("Coffee", "coffee.jpg", 2.00, "...", 5);	  

-- sub_foods INSERT
INSERT INTO sub_foods (
sub_name, sub_price, food_id
)
VALUE ("extra tomato", 0.50, 1),
	  ("extra chicken", 0.50, 2),
	  ("french fries", 0.50, 4),
	  ("ice", 0.00, 8),
	  ("ice", 0.00, 9),
	  ("extra soup", 0.25, 1),
	  ("extra soup", 0.25, 2),
	  ("extra vegetable", 1.50, 6),
	  ("extra pasta", 2.00, 6);

-- users INSERT
INSERT INTO users (
full_name, email, `password`
)
VALUE ("Alice Nguyen", "alice_nguyen@gmail.com", "alice_nguyen_pass"),
	  ("John Nguyen", "john_nguyen@gmail.com", "john_nguyen_pass"),
	  ("Bill Nguyen", "bill_nguyen@gmail.com", "bill_nguyen_pass"),
	  ("Jackson Nguyen", "alice_nguyen@gmail.com", "alice_nguyen_pass"),
	  ("Justin Le", "justin_le@gmail.com", "justin_le_pass"),
	  ("Michael Nguyen", "michael_nguyen@gmail.com", "michael_nguyen_pass"),
	  ("Tony Tran", "tony_tran@gmail.com", "tony_tran_pass");

-- orders INSERT
INSERT INTO orders (
user_id, food_id, amount, `code`, arr_sub_id
)
VALUE (1, 1, 2, "", '[1, 1]'),
	  (1, 3, 1, "", '[]'),
	  (2, 4, 1, "", '[3, 3]'),
	  (3, 9, 3, "Awake Monday", '[5, 5, 5]');

-- restaurants INSERT
INSERT INTO restaurants (
res_name, image, `desc`
)
VALUE ("Rustic BBQ", "rustic-restaurant.jpg", "..."),
	  ("SG Sidewalk", "sg-sidewalk-restaurant.jpg", "..."),
	  ("Lorem Ipsum", "lorem-restaurant.jpg", "..."),
	  ("Ichigo", "ichigo-restaurant.jpg", "..."),
	  ("Burgatory", "burgatory-restaurant.jpg", "...");

-- rate_res INSERT
INSERT INTO rate_res (
user_id, res_id, amount, date_rate
)
VALUE (1, 3, 5, "2022-01-11"),
	  (2, 1, 5, "2022-10-21"),
	  (3, 2, 4, "2022-11-11");

-- like_res INSERT
INSERT INTO like_res (
user_id, res_id, date_like
)
VALUE (1, 2, "2022-01-11"),
  	  (1, 3, "2022-02-26"),
  	  
	  (2, 1, "2022-03-01"),
	  (2, 2, "2022-03-15"),
	  (2, 4, "2022-03-15"),
	  
	  (3, 1, "2022-03-16"),
	  (3, 2, "2022-04-26"),
	  
	  (4, 3, "2022-04-30"),
	  (4, 4, "2022-05-10"),
	  
	  (5, 1, "2022-05-24"),
	  (5, 2, "2022-06-01"),
	  (5, 3, "2022-06-12"),
	  
	  (6, 2, "2022-06-15");


-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- Retrieve data -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --


-- 5 người đã like nhiều nhất
SELECT users.full_name as `Full name`,
	   count(like_res.user_id) as `Total like`
	   
FROM like_res 
INNER JOIN users
ON users.user_id = like_res.user_id

GROUP BY like_res.user_id
ORDER BY `Total like` desc
LIMIT 5;



-- 2 nhà hàng có lượt like nhiều nhất
SELECT restaurants.res_name as Restaurant,
	   count(like_res.res_id) as `Total like`
	   	
FROM restaurants
INNER JOIN like_res
ON restaurants.res_id = like_res.res_id

GROUP BY like_res.res_id
LIMIT 2;



-- Người dùng đã đặt hàng nhiều nhất
SELECT users.full_name as `Full name`,
	   count(orders.user_id) as `Total order`
	   
FROM users
INNER JOIN orders
ON users.user_id = orders.user_id

GROUP BY orders.user_id
ORDER BY `Total order` desc 
LIMIT 1;



-- Người dùng không hoạt động trong hệ thống
SELECT users.user_id as `User ID`,
	   users.full_name as `Full name`,
	   orders.user_id as `Order`,
	   rate_res.user_id as `Rate`,
	   like_res.user_id as `Like`

FROM users
LEFT JOIN orders
ON users.user_id = orders.user_id
LEFT JOIN rate_res
ON users.user_id = rate_res.user_id
LEFT JOIN like_res
ON users.user_id =like_res.user_id

WHERE   orders.user_id is null &&
	  rate_res.user_id is null &&
	  like_res.user_id is null;



-- Trung bình
-- Trung bình số món phụ trên một món ăn
SELECT count(foods.food_id) as `Total food`,
	   count(sub_foods.sub_id) as `Total sub food`,
	   count(foods.food_id) / count(sub_foods.sub_id) as `Sub food per food`

FROM foods
LEFT JOIN sub_foods
ON foods.food_id = sub_foods.food_id;
-- Trung bình giá món phụ của từng món ăn
SELECT foods.food_name as `Food name`,
	   count(sub_foods.food_id) as `Numbers of sub`,
	   avg(sub_foods.sub_price) as `Average sub price of this food`
	   
FROM foods
INNER JOIN sub_foods
ON foods.food_id = sub_foods.food_id

GROUP BY foods.food_id;




